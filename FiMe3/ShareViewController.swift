//
//  ShareViewController.swift
//  FiMe3
//
//  Created by Ignas Bobinas on 12/15/14.
//  Copyright (c) 2014 Ignas Bobinas. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var buttonView: UIButton!
    var selectedCoordinate:CLLocationCoordinate2D!
    var mapMarker: GMSMarker!
    var body: String!
    
    override func viewDidLoad() {
        mapView.addSubview(buttonView)
        mapView.bringSubviewToFront(buttonView)

        super.viewDidLoad()
        
        self.mapView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func shareClicked(sender: AnyObject) {
        
    }
    
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        mapMarker = GMSMarker()
        mapMarker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        mapMarker.map = mapView
        
        selectedCoordinate = coordinate
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if selectedCoordinate != nil {
            NSLog("CLICKED ENTERED")
            
            var notification = PFObject(className:"Notification")
            
            let obj:[String:AnyObject] = [
                "message": "Message From iOS app",
                "coordinate_x": selectedCoordinate.longitude,
                "coordinate_y": selectedCoordinate.latitude,
                "time": "15:00"
            ]
            body = JSON(obj).toString()
            NSLog(body)
            if segue.identifier == "goto_friend" {
                var destinationController:FriendsTableViewController = segue.destinationViewController as FriendsTableViewController
                destinationController.messageBody = body
            }
        }
        
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if selectedCoordinate != nil {
            return true
        }
        return false;
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
