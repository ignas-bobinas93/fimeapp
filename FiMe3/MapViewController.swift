//
//  MapViewController.swift
//  FiMe3
//
//  Created by Ignas Bobinas on 12/9/14.
//  Copyright (c) 2014 Ignas Bobinas. All rights reserved.
//

import UIKit

class MapViewController: UIViewController, UITextFieldDelegate{
    

    @IBOutlet weak var timeView: UILabel!
    @IBOutlet weak var messageView: UITextView!
    @IBOutlet weak var mapView: GMSMapView!
    var messageBody: String = ""
    
    override func viewDidLoad() {
        mapView.addSubview(timeView)
        mapView.addSubview(messageView)
        mapView.bringSubviewToFront(timeView)
        mapView.bringSubviewToFront(messageView)
        
        super.viewDidLoad()
        let json = JSON(string:messageBody)
        timeView.text = json["time"].asString!
        messageView.text = json["message"].asString!
        let marker = GMSMarker()
        var coordinateX = json["coordinate_x"].asDouble!
        var coordinateY = json["coordinate_y"].asDouble!

        marker.position = CLLocationCoordinate2DMake(coordinateY, coordinateX)
        marker.map = mapView
        mapView.camera = GMSCameraPosition(target: marker.position, zoom: 15, bearing: 0, viewingAngle: 0)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
