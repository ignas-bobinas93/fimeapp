//
//  MainViewControllerTableViewController.swift
//  FiMe3
//
//  Created by Ignas Bobinas on 12/8/14.
//  Copyright (c) 2014 Ignas Bobinas. All rights reserved.
//

import UIKit
import Foundation

class MainViewControllerTableViewController: UITableViewController {

    var someInts = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("SEAGUE LOADED")
        var nib = UINib(nibName: "TableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "main_table_cell")
        
        var query = PFQuery(className:"Notification")
        var currentUser = PFUser.currentUser().objectId
        query.whereKey("recipientId", equalTo:currentUser)
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                self.someInts = objects
                self.tableView.reloadData()
            } else {
                // Log details of the failure
                NSLog("Error: %@ %@", error, error.userInfo!)
            }
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    @IBOutlet var mainTableView: UITableView!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return someInts.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("main_table_cell") as TableCellTableViewCell
        var notification:PFObject = someInts[indexPath.row] as PFObject
        var user:PFUser = notification["sender"].fetchIfNeeded() as PFUser
        cell.nameLabel.text = user.username

        var message:String = notification["body"] as String
        let json = JSON(string:message)
        cell.timeLabel.text = json["time"].asString!
       
        var imageName = UIImage(named: "ic_action_earth")
        cell.ImageView.image = imageName
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("goto_map", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "goto_map") {
            // pass data to next view
            var destination: MapViewController = segue.destinationViewController as MapViewController
            var row = self.tableView.indexPathForSelectedRow()?.row
            
            var notification:PFObject = someInts[row!] as PFObject
            destination.messageBody = notification["body"] as String
            

        } else if (segue.identifier == "logout"){
            PFUser.logOut()
            var currentUser = PFUser.currentUser()
        }
    }
    
    // MARK: - Table view data source

   }
