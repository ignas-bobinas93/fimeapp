//
//  ViewController.swift
//  FiMe3
//
//  Created by Ignas Bobinas on 12/2/14.
//  Copyright (c) 2014 Ignas Bobinas. All rights reserved.
//

import UIKit



class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var labelLogger: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        loginInput.delegate = self
        passwordInput.delegate = self


        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginAction(sender: AnyObject) {
        var username = self.loginInput.text
        var password = self.passwordInput.text
        
        PFUser.logInWithUsernameInBackground(username, password: password) {
            (user: PFUser!, error: NSError!) -> Void in
            if user != nil {
                // Do stuff after successful login.
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("goto_main", sender: self)
                }
            } else {
                // The login failed. Check error to see why.
            }
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }

}

